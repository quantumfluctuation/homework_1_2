1. 功能点：
* 基于注解的IOC
    1. @Service注解标识类为bean对象，接受IOC容器的管理，需要提供无参构造器，value属性可以指定（不能重复）
    2. @Autowired注解标识类的属性，IOC容器自动注入实例。value属性不指定，则按类全名匹配，若指定，则按名称匹配
        因此，若声明为接口，则需指定value
* 基于注解的事务控制
    1. @Transactional注解标识需要进行事务控制的方法
            有接口的话，需要在接口方法中标注？这个问题需要解决
2. 实现原理：
    1. web.xml配置监听类，在web工程启动时 启动IOC和AOP功能
    2. 根据bean.xml配置的包路径，扫描所有的类
    3. 扫描类上的@Service，调用无参构造器实例化
    4. 扫描属性中的@Autowired，将实例化后bean注入
    5. 用代理类替换所有的bean，代理类中判断方法有无@Transactional，完成事务控制