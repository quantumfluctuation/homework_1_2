package com.lagou.edu.utils;

import com.lagou.edu.annotation.Service;

import java.util.Map;
import java.util.stream.Collectors;

public class AnnotationUtils {
    public static Map<String, Class<?>> getServiceBeans(String packagePath) {
        return ClassUtils.getClasssFromPackage(packagePath).stream()
                .filter(clazz -> clazz.isAnnotationPresent(Service.class))
                .collect(Collectors.toMap(clazz -> {
                    String value = clazz.getAnnotation(Service.class).value();
                    if ("".equals(value)) {
                        return clazz.getName();
                    } else {
                        return value;
                    }
                }, clazz -> clazz));
    }

    public static void main(String[] args) {
        Map<String, Class<?>> classMaps = getServiceBeans("com.lagou.edu");
        System.out.println(classMaps.toString());
    }

}
