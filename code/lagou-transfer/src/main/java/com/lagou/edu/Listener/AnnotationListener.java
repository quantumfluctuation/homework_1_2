package com.lagou.edu.Listener;

import com.lagou.edu.annotation.Autowired;
import com.lagou.edu.factory.BeanFactory;
import com.lagou.edu.factory.ProxyFactory;
import com.lagou.edu.pojo.BeanField;
import com.lagou.edu.utils.AnnotationUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AnnotationListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("AnnotationListener initialized");
        try {
            //1.获取xml中配置的包扫描路径
            String packagePath = getPackagePathFromXML();
            //2.获取包路径下所有Class对象
            Map<String, Class<?>> beans = AnnotationUtils.getServiceBeans(packagePath);
            //3.将Class对象实例化
            Map<String, Object> beanMap = new HashMap<>();
            for (String beanName : beans.keySet()) {
                Class<?> clazz = beans.get(beanName);
                //调用无参构造器创建实例
                Object bean = clazz.newInstance();
                //创建代理对象
                beanMap.put(beanName, bean);
            }
            //4.遍历bean，对属性注入bean
            Map<String, List<BeanField>> dependCache = new HashMap<>();
            for (String beanName : beanMap.keySet()) {
                Object bean = beanMap.get(beanName);
                for (Field field : bean.getClass().getDeclaredFields()) {
                    //有Autowired注解的属性即需要注入
                    if (field.isAnnotationPresent(Autowired.class)) {
                        //获取Autowired的value，即bean的名称
                        String autowiredBeanName = field.getAnnotation(Autowired.class).value();
                        //如果是默认值""，则bean的名称取类全名
                        autowiredBeanName = autowiredBeanName.equals("") ? field.getGenericType().getTypeName() : autowiredBeanName;
                        //获取到注入的bean
                        Object autowiredBean = beanMap.get(autowiredBeanName);
                        //注入到本属性中
                        field.setAccessible(true);
                        field.set(bean, autowiredBean);
                        System.out.println("autowiredBean:" + autowiredBean);
                        //维护bean的依赖关系
                        if (dependCache.containsKey(autowiredBeanName)) {
                            dependCache.get(autowiredBeanName).add(new BeanField(bean, field));
                        } else {
                            dependCache.put(autowiredBeanName, new ArrayList<>() {
                                {
                                    add(new BeanField(bean, field));
                                }
                            });
                        }
                    }
                }
            }

            //5.遍历bean，用代理类替换
            // TODO 这里可以优化为只有用了@Transactional的类才用代理类替换掉
            // PS：为啥不在 3直接使用代理类，因为代理类本身用了bean注入，啊啊啊
            for (String beanName : beanMap.keySet()) {
                System.out.println("beanName:" + beanName);
                Object bean = beanMap.get(beanName);
                ProxyFactory proxyFactory = (ProxyFactory) beanMap.get(ProxyFactory.class.getName());
                //TODO 这边判断不严谨
                Object proxyBean = null;
                //实现接口采用jdk
                if (bean.getClass().getInterfaces() != null && bean.getClass().getInterfaces().length != 0) {
                    proxyBean = proxyFactory.getJdkProxy(bean);
                }
                //没有实现接口采用cglib
                else {
                    proxyBean = proxyFactory.getCglibProxy(bean);
                }
                //代理对象替换实际对象
                beanMap.put(beanName, proxyBean);
                //循环被依赖的对象，将bean替换为代理后的bean
                if (dependCache.get(beanName) != null) {
                    for (BeanField beanField : dependCache.get(beanName)) {
                        beanField.getField().set(beanField.getBean(), proxyBean);
                    }
                } else {
                    System.out.println("beanName:" + beanName + "=null");
                }
            }

            //6.赋值给BeanFactory
            Field map = BeanFactory.class.getDeclaredField("map");
            map.setAccessible(true);
            map.set(null, beanMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }

    private String getPackagePathFromXML() throws DocumentException {
        InputStream resourceAsStream = BeanFactory.class.getClassLoader().getResourceAsStream("beans.xml");
        SAXReader saxReader = new SAXReader();
        Document document = saxReader.read(resourceAsStream);
        Element rootElement = document.getRootElement();
        return rootElement.attributeValue("package");
    }
}
