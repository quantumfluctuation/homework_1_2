package com.lagou.edu.pojo;

import java.lang.reflect.Field;

public class BeanField {
    private Object bean;
    private Field field;

    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        this.bean = bean;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public BeanField(Object bean, Field field) {
        this.bean = bean;
        this.field = field;
    }
}
