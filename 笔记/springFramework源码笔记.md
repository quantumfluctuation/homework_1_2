#概念
#语法
##bean注入的4种方式
1. 构造器注入
```
public class LagouBean {

	private ItBean itBean;

	public LagouBean(ItBean itBean){
		System.out.println("LagouBean 构造器...");
	}
}
```
2. set注入
```
public class LagouBean{
	private ItBean itBean;

	public void setItBean(ItBean itBean) {
		this.itBean = itBean;
	}
}
```
3. 静态工厂方法注入
```
public class lagouBeanFactory {

	public static LagouBean getLagouBean(){
		return new LagouBean();
	}
}
```
```
<bean id="lagouBean" class="com.lagou.factory.lagouBeanFactory" factory-method="getLagouBean"></bean>
```
4. 实例工厂方法注入
```
public class lagouBeanFactory {

	public LagouBean getLagouBean(){
		return new LagouBean();
	}
}
```
```
<bean id="lagouBeanFactory"class="com.lagou.factory.instancemethod.lagouBeanFactory"></bean>
<bean id="lagouBean" factory-bean="lagouBeanFactory" factory-method="getTransferService"></bean>
```
##bean初始化过程中的扩展方法 [spring ioc 扩展点](https://www.jianshu.com/p/397c15cbf34a)
BeanFactory后置处理器
```
public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

	public MyBeanFactoryPostProcessor() {
		System.out.println("BeanFactoryPostProcessor的实现类构造函数...");
	}

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		System.out.println("BeanFactoryPostProcessor的实现方法调用中......");
	}
}
```
Bean后置处理器
```
public class MyBeanPostProcessor implements BeanPostProcessor {

	public MyBeanPostProcessor() {
		System.out.println("BeanPostProcessor 实现类构造函数...");
	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		if("lagouBean".equals(beanName)) {
			System.out.println("BeanPostProcessor 实现类 postProcessBeforeInitialization 方法被调用中......");
		}
		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if("lagouBean".equals(beanName)) {
			System.out.println("BeanPostProcessor 实现类 postProcessAfterInitialization 方法被调用中......");
		}
		return bean;
	}
}
```
Bean set注入后的
```
public class LagouBean implements InitializingBean, ApplicationContextAware {

	private ItBean itBean;

	public void setItBean(ItBean itBean) {
		this.itBean = itBean;
	}

	public LagouBean(){
		System.out.println("LagouBean 构造器...");
	}


	/**
	 * InitializingBean 接口实现
	 */
	public void afterPropertiesSet() throws Exception {
		System.out.println("LagouBean afterPropertiesSet...");
	}

	public void print() {
		System.out.println("print方法业务逻辑执行");
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		System.out.println("setApplicationContext....");
	}
}
```
#Spring容器、BeanFactory和ApplicationContext [参考博客](https://www.jianshu.com/p/2854d8984dfc)
1. spring容器=IOC容器
2. BeanFactory接口，这是最简单的容器，只能提供基本的DI功能，DefaultListableBeanFactory是其实现
3. 继承自BeanFactory接口的ApplicationContext接口，它能提供更多企业级的服务
    * 默认初始化所有的Singleton，也可以通过配置取消预初始化
    * 继承MessageSource，因此支持国际化
    * 资源访问，比如访问URL和文件
    * 事件机制
    * 同时加载多个配置文件
    * 以声明式方式启动并创建Spring容器
* 图1：BeanFactory的继承体系 ![avatar](BeanFactory.png)
* 图2：DefaultListableBeanFactory类图 ![avatar](XmlBeanFactory.png)
* 图3：XmlBeanDefinitionReader类图 
    * 通过继承AbstractBeanDefinitionReader，使用ResourceLoader将文件路径转化为Resource
    * DocumentLoader加载Resource为Document
    * 通过DefaultBeanDefinitionDocumentReader对Document解析，DefaultBeanDefinitionDocumentReader通过BeanDefinitionParseDelegate解析Element
![avatar](XmlBeanDefinitionReader.png)

5. 其他接口待补充...

#XmlBeanFactory使用
 XmlBeanFactory在spring 3.1已不推荐使用，但是因为实现简单，降低理解spring ioc难度
```
//使用xml初始化BeanFactory
BeanFactory bf = new XmlBeanFactory(new ClassPathResource("applicationContext.xml"));
//从BeanFactory中拿取bean
UserService userService = (UserService) bf.getBean("userService");  
```
```
//替代XmlBeanFactory的方式
BeanFactory bf=new DefaultListableBeanFactory();  
BeanDefinitionReader bdr=new XmlBeanDefinitionReader((BeanDefinitionRegistry) bf);  
bdr.loadBeanDefinitions(new ClassPathResource("applicationContext.xml")); 
```
##BeanFactory初始化
1. 加载xml->inputStream->Document
2. 将xml中的<bean>解析为BeanDefinition对象注册到DefaultListableBeanFactory.beanDefinitionMap中，其中标签的解析是难点 @ TODO
##bean的初始化/实例化
1. 实例化比标签解析还要复杂，@ TODO
#ApplicationContext使用
```
ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
UserService userService = (UserService) ac.getBean("userService");
```

#spring IOC容器初始化流程     
* 主体流程在AbstractApplicationContext.refresh()方法中
```
@Override
public void refresh() throws BeansException, IllegalStateException {
    synchronized (this.startupShutdownMonitor) {
        // Prepare this context for refreshing.
        // 刷新前的准备工作，即准备和验证环境信息/上下文环境
        prepareRefresh();

        // Tell the subclass to refresh the internal bean factory.
        // 初始化BeanFactory并对xml文件进行读取
        ConfigurableListableBeanFactory beanFactory = obtainFreshBeanFactory();

        // Prepare the bean factory for use in this context.
        // 对beanFactory进行功能填充
        prepareBeanFactory(beanFactory);

        try {
            // Allows post-processing of the bean factory in context subclasses.
            //扩展点：模板设计模式
            postProcessBeanFactory(beanFactory);

            // Invoke factory processors registered as beans in the context.
            //激活beanFactory的后置处理器，这步调用了BeanFactoryPostProcessor
            invokeBeanFactoryPostProcessors(beanFactory);

            // Register bean processors that intercept bean creation.
            //注册BeanPostProcessors，又是一个扩展点
            registerBeanPostProcessors(beanFactory);

            // Initialize message source for this context.
            // 初始化MessageSource，国际化支持
            initMessageSource();

            // Initialize event multicaster for this context.
            // 初始化应用消息广播器，并放入applicationEventMulticaster的bean中
            initApplicationEventMulticaster();

            // Initialize other special beans in specific context subclasses.
            // 子类扩展点，子类初始化其他的bean
            onRefresh();

            // Check for listener beans and register them.
            // 在注册的bean中找到listener，注册到广播器中
            registerListeners();

            // Instantiate all remaining (non-lazy-init) singletons.
            //singletons & non-lazy-init bean的初始化
            finishBeanFactoryInitialization(beanFactory);

            // Last step: publish corresponding event.
            // 完成刷新过程，通知lifecycleProcessor，同时发出contextRefresh通知别人
            finishRefresh();
        }

        catch (BeansException ex) {
            if (logger.isWarnEnabled()) {
                logger.warn("Exception encountered during context initialization - " +
                        "cancelling refresh attempt: " + ex);
            }

            // Destroy already created singletons to avoid dangling resources.
            destroyBeans();

            // Reset 'active' flag.
            cancelRefresh(ex);

            // Propagate exception to caller.
            throw ex;
        }

        finally {
            // Reset common introspection caches in Spring's core, since we
            // might not ever need metadata for singleton beans anymore...
            resetCommonCaches();
        }
    }
}
```

#aop
aop的解决了什么问题
* oop编程代码复用问题，子类父类代码复用OK，但是横向的不能很好解决，
* 例如:非业务的像：日志，事务，权限，性能监控等与业务代码分离
* aop可以在不改变原有代码情况下，无侵入的增强原有代码
##aop与代理
1. 代理的两种：静态代理和动态代理
2. 动态代理的两种实现：jdk的动态代理和cglib
    有接口，jdk，一样
    无接口，cglib：第一次漫生成字节码，之后快
3. 语法
    * xml
        ```
        <bean id="logUtil" class="com.lagou.utils.LogUtil"></bean>
        <!--开始aop的配置-->
        <aop:config>
        <!--配置切⾯-->
        <aop:aspect id="logAdvice" ref="logUtil">
        <!--配置前置通知-->
        <aop:before method="printLog" pointcut="execution(public * com.lagou.service.impl.TransferServiceImpl.updateAccountByCardNo(com.lagou.pojo.Account))"></aop:before>
        </aop:aspect>
        </aop:config>
        ```
    * xml+注解
        * xml开启aop注解
            ```
            <!--开启spring对注解aop的⽀持-->
            <aop:aspectj-autoproxy/>
            ```
        * java注解配置切面
            ```
            @Component
            @Aspect
            public class LogUtil {
                @Pointcut("execution(* com.lagou.service.impl.*.*(..))")
                public void pointcut(){}
                
                @Before("pointcut()")
                public void beforePrintLog(JoinPoint jp){
                    Object[] args = jp.getArgs();
                    System.out.println("前置通知： beforePrintLog，参数是： "+
                    Arrays.toString(args));
                }
            }
            ```
    * 注解
        * 使用javaConfiguration开启aop
            ```
            @Configuration
            @ComponentScan("com.lagou")
            @EnableAspectJAutoProxy //开启spring对注解AOP的⽀持
            public class SpringConfiguration {
            }
            ```
        * java注解配置切面
#声明式事务
##声明式和编程式
##事务的四大特性
##事务问题
* 脏读：⼀个线程中的事务读到了另外⼀个线程中未提交的数据。
* 不可重复读：⼀个线程中的事务读到了另外⼀个线程中已经提交的update的数据（前后内容不⼀样）
    ```
    员⼯A发起事务1，查询⼯资，⼯资为1w，此时事务1尚未关闭
    财务⼈员发起了事务2，给员⼯A张了2000块钱， 并且提交了事务
    员⼯A通过事务1再次发起查询请求，发现⼯资为1.2w，原来读出来1w读不到了，叫做不可重复读
    ```
* 虚读（幻读）：⼀个线程中的事务读到了另外⼀个线程中已经提交的insert或者delete的数据（前后条数不⼀样）
    ```
    事务1查询所有⼯资为1w的员⼯的总数，查询出来了10个⼈，此时事务尚未关闭
    事务2财务⼈员发起，新来员⼯，⼯资1w，向表中插⼊了2条数据， 并且提交了事务
    事务1再次查询⼯资为1w的员⼯个数，发现有12个⼈，⻅了⻤了
    ```
##事务的隔离级别
* Serializable（串⾏化）：可避免脏读、不可重复读、虚读情况的发⽣。
* Repeatable read（可重复读）：可避免脏读、不可重复读情况的发⽣。幻读有可能发⽣。 该机制下会对要update的⾏进⾏加锁
* Read committed（读已提交）：可避免脏读情况发⽣。不可重复读和幻读⼀定会发⽣。 
* Read uncommitted（读未提交）：最低级别，以上情况均⽆法保证。 
MySQL的默认隔离级别是： REPEATABLE READ
oracle默认Repeatable read
##事务的传播方式：service层，a方法调b方法，事务传播
声明式事务采用代理方式实现，同一个类中的方法调用，事务注解无效
* PROPAGATION_REQUIRED 如果当前没有事务，就新建⼀个事务，如果已经存在⼀个事务中，加⼊到这个事务中。这是最常⻅的选择。
* PROPAGATION_SUPPORTS ⽀持当前事务，如果当前没有事务，就以⾮事务⽅式执⾏。
* PROPAGATION_MANDATORY 使⽤当前的事务，如果当前没有事务，就抛出异常。
* PROPAGATION_REQUIRES_NEW 新建事务，如果当前存在事务，把当前事务挂起。
* PROPAGATION_NOT_SUPPORTED 以⾮事务⽅式执⾏操作，如果当前存在事务，就把当前事务挂起。
* PROPAGATION_NEVER 以⾮事务⽅式执⾏，如果当前存在事务，则抛出异常。
* PROPAGATION_NESTED 如果当前存在事务，则在嵌套事务内执⾏。如果当前没有事务，则执⾏与PROPAGATION_REQUIRED类似的操作。
##spring事务
1. spring制定接事务管理器的接口，用户可以自己指定，内置了
    * DataSourceTransactionManager：Spring JdbcTemplate，Mybatis
    * HibernateTransactionManager：Hibernate




